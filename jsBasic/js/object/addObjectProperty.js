/*Write the following code, one line for each action:
Create an empty object product
Add the property name with the value ’Laptop’
Add the property price with the value 1200
Change the value of the price to 1000
Show the product’s name and price on the screen
Remove the property name from the object
codeacademy
htmlacademy
gulp webdesign 
http://192.168.13.171:3000/
*/

//Write functions, that universaly ask user new object value and property, and add it to your object

function askProperty(functionName, objectName){
let nameProperty = prompt('Please, type the property name');
    if(nameProperty != null){
      let valueProperty = prompt('Please, type the value ');
        functionName(nameProperty,valueProperty, objectName);
        return true;
//     return   let productValue = [nameProperty, valueProperty];
    } 
    return false;
};

function addProperty(name, value, objectName){
    objectName[name] = value; 
    //objectName.name = value; 
    return true;
};

//let productValue = askProperty();
//addProperty(productValue);

let product = {};

product.name = 'laptop';
product.price = 1200;

askProperty(addProperty, product);
console.log(product);

