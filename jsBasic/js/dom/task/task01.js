function changeCssProprty(elementsArray, whichElement, propertyName, propertyValue) {

    if (elementsArray[0].style[propertyName] === undefined) {
        throw new Error("! no property");
        console.error("propertyName == no property");
        return false;
    }

    for (let i = 0; i < elementsArray.length; i += whichElement) {
        elementsArray[i].style[propertyName] = propertyValue;
    }
    return false;
}



let cells = document.getElementsByTagName('td');
let elemN = 5;
let propertyName = "backgroundColor";
let color = 'green';
changeCssProprty(cells, elemN, propertyName, color);
