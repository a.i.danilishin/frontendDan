function changeStyle (property, value) {
    let newStyle = this.style[property].slice(0, this.style[property].indexOf('px'));
    newStyle = Number(newStyle) + value + 'px';
    this.style[property] = newStyle;
    return newStyle;
}

let elem = document.getElementById("text-container");  // Получаем id тега
let style = getComputedStyle(elem);

changeStyle.call(elem, "margin", 40 );