/*
 Create an object calculator with three methods:
→    read() prompts for two values and saves them as object properties
→    sum() returns the sum of saved values
→    mul() multiplies saved values and returns the result
let calculator = {
// ... your code ...
};

calculator.read();  alert(calculator.sum());  alert(calculator.mul());*/
let calculator =  {
    read: function () {
        this.one = Number(prompt("enter A"));
        this.two = Number(prompt("enter B"));
    },
    sum: function () {
        return Number(this.one + this.two);
    },
    mult: function () {
        return Number(this.one * this.two);
    }
}


calculator.read();

console.log(calculator);
