/*
Найти факториал числа, у пользователя запрашивается число, и в результате выдаётся факториал числа.
Если ввести к примеру 5 то результат должен быть равен 120, потому что факториал 5 это 5*4*3*2*1.

*/
function factorial(number) {
    let factorial = 1;
    for (let i = 1; i <= number; i++) {
        factorial *= i;
    }
    return factorial;
}

let number = prompt("Number");
number=Number(number);
console.log(factorial(number));
