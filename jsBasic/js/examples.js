let data = [1, 2, 3, 10, 8];
let input = ["d12345", "d2", "d", "d4"];
let inputCopy = [];

data.forEach(el => console.log(el));
data.forEach(
    function (elem) {
        console.log(elem);
    });

input.forEach(el => (inputCopy.push(el)));
inputCopy.forEach(el => console.log(el));

function logArrayElements(el, index, arr) {
    console.log('a[' + index + '] = ' + el);
}
[2, 5, , 9].forEach(logArrayElements);

function roundNumber(number, digits) {
    return parseFloat(number).toFixed(digits);
}
console.log(roundNumber(1.2658, 2));

