/*Ask the user to enter a number
If the user provides a non-numeric value (such as “abc”), display an error message and ask the user to try again
Hint: use the function isNaN() to check if the conversion to number failed*/

let number = prompt("Enter a number", "");

while(!Number.isNaN(number)){
   number = prompt("Error, Enter a number", "")
}

console.log(number);