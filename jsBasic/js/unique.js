/*
Let arr be an array
Create a function unique(arr) that should return an array with unique items of arr
Use set to make the function more efficient
For instance:
function unique(arr) {
 your code 
}
let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];  alert(unique(values)); // John, Harry, Mary, Beth
*/


function uniqueSet(arr) {    
    return Array.from(new Set(arr));
}

function uniqueArr(arr) {
    let uniqueArr = new Array();
    for (let i = 0; i < arr.length; i++) {
        if (!uniqueArr.includes(arr[i])) {
            uniqueArr.push(arr[i]);
        }
    }
    return uniqueArr;
}

let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];

console.log(uniqueArr(values));
console.log(uniqueSet(values));
