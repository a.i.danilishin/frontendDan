
/*Write a function sumInput() that:
Asks the user for values using prompt and stores the values in the array
Finishes asking when the user enters a non-numeric value, an empty string, or presses  “Cancel”
Calculates and returns the sum of array items*/

function sumInput(arr){    
    return arr.reduce(function(sum, current){return sum+current},0);
}

let nums = [10, 20, 30, 40, 50];
console.log(sumInput(nums));

/*function getUsrPromptOrFalse(){
    let numbers = [];
    while()
    
}
let input =prompt(sumInput());*/
