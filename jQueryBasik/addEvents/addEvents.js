/*You have a page with three links
Using jQuery:
Add a button next to each link
The button’s text should be equal to the text of the link
Clicking the button should change the window’s location to the address of the link*/


$("a").each(function () {
    $(this).append("<button class='yahoo'>" + $(this).text() + "</button>");
});

