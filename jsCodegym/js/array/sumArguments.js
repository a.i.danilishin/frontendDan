function sum() {
  let arg = Array.from(arguments)
  const reducer = (accumulator, currentValue) => accumulator + currentValue; 
    let sum =  arg.reduce(reducer);
    return sum;
}

sum([ 1, 4, 10, 2 ]);