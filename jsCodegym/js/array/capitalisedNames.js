function makeFirstSymbolUpperCase(arrayList) {
    let newArray = [];
    arrayList.forEach(function (el) {
        newArray.push(el.substr(0, 1).toUpperCase() + el.substr(1))
    });
    return newArray;
}
