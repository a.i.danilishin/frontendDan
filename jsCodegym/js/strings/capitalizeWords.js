function capitalizeWords(str) {
    let words = str.split(' ');   
    let wordsStr =[];
    words.forEach(function (item) {
        wordsStr.push(item.substr(0, 1).toUpperCase() + item.substr(1).toLocaleLowerCase());       
    });
    let capitalizedStr = wordsStr.join(' ');
    return capitalizedStr;
}

function titleCase(str) {
 let words = str.split(' ');    
    let wordsStr =[];
    words.forEach(function (item) {
        wordsStr.push(item.substr(0, 1).toUpperCase() + item.substr(1).toLowerCase());     
    });
    let  cleared= wordsStr.join(' ');
    return cleared;
}




capitalizeWords("capitalize words");
