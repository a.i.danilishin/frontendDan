function sumNumbers(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        if (!isNaN(arr[i])) {
            sum += Number(arr[i]);
        }

    }
    return sum;
}

let num = [1, "2", 3, "ads", 4, 5];
console.log(sumNumbers(num));


