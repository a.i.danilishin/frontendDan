
function addNumbers(numA, numB) {
    numA = Number(numA);
    numB = Number(numB);
    if(Number.isNaN(numA) || Number.isNaN(numB) ){
        return false
    }else{
        return numA + numB;
    }
}