function fizzBuzz() {
    let array = [];
    for (i = 0; i < 100; i++) {
        if (i % 3 == 0 && i % 5 == 0) {
            array.push('FizzBuzz');
            continue;
        } else if (i % 5 == 0) {
            array.push('Buzz');
        } else if (i % 3 == 0) {
            array.push('Fizz');
        } else {
            array.push(i);
        }
    }
    array.forEach(function (element) {
        console.log(element);
    });
}