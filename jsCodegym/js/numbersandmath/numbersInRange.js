function sumRange(from, to) {
    let sum = 0;
    if (from < to) {
        for (let i = from; i <= to; i++) {
            sum += i;
        }
        return sum;
    }else{
        for (let i = to; i <= from; i++) {
            sum += i;
        }
        return sum;
    }
}
